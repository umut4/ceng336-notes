# Interrupts
## Priority Levels
Priority of interrupts are enabled by setting `IPEN (RCON<7>)` bit. 
- **High Priority:**
  - Its interrupt vector is at `0008h`
  - Can interrupt a low priority ISR.
- **Low Priority**
  - Its interrupt vector is at `0018h`

## INTCON Register
Register `INTCON` controls the interrupt logic.
- **Bit 7:** `Global Interrupt Enable bit (GIE, INTCON<7>)`:
  - It is called .
  - If priority is enabled:
    - If bit 7 is set, all high-priority interrupts are enabled.
    - Otherwise, all interrupts are disabled.
  - If priority is disabled:
    - If bit 7 is set, all unmasked interrupts are enabled.
    - Otherwise, all interrupts are disabled.
  - DO NOT set it until program is fully initialized.
- **Bit 6:** `Peripheral Interrupt Enable bit (PEIE, INTCON<6>)`:
  - If priority is enabled:
    - If bit 6 is set, all low-priority peripheral interrupts are enabled.
    - Otherwise, all low-priority peripheral interrupts are disabled.
  - If priority is disabled:
    - If bit 6 is set, all unmasked peripheral interrupts are enabled.
    - Otherwise, all peripheral interrupts are disabled.
- **Bit 5:** `TMR0 Overflow Interrupt Enable bit (TMR0IE, INTCON<5>)`
- **Bit 4:** `INT0 External Interrupt Enable bit (INT0IE, INTCON<4>)`
- **Bit 3:** `RB Port Change Interrupt Enable bit (RBIE, INTCON<3>)`
  - Port B bits 4-7 should be used as interrupt sources.
  - Only the pins that are configured as inputs can trigger this interrupt.
- **Bit 2:** `TMR0 Overflow Interrupt Flag bit (TMR0IF, INTCON<2>)`
- **Bit 1:** `INT0 External Interrupt Flag bit (INT0IF, INTCON<1>)`
- **Bit 0:** `RB Port Change Interrupt Flag bit (RBIF, INTCON<0>)`
  - If RBIF is set, then at least one of RB7:RB4 changed state.

## When an Interrupt is Received
- When an interrupt is received, according to the flag, a polling must be made to decide on the device that caused the interrupt.
- After an ISR, interrupt flags must be cleared by the programmer, otherwise PIC will go into an infinite interrupt loop.

## External Interrupts
- On `RB0/INT0`:
  - The pin `RB0/INT0` is set on external interrupts.
  - The bit `INTEDG0` in `INTCON2` determines whether external interrupts will be triggerd on the rising edge (if == 1 ) or falling edge (if == 0).
  - `INT0IF` bit (INTCON<1>) must be cleared in the ISR, otherwise interrupt will happen again.
  
## Initializing Interrputs
- Set the relevant enable bits to get interrupts from the resources that we want.
- Example initialization:
```
clrf  INTCON        ; all interrupts are disabled at the beginning
movlw B'00010000'
movwf INTCON        ; this enables the external interrupt only
bsf   INTCON, GIE   ; Finally, enable all interrupts
  ```

# Interrupt Service Routine (ISR)
  - When it's done, clears the interrupt flag.
  - It should save register contents that may be affected by the routine.
  - Must be terminated by `RETFIE` instruction.
  - When an interrupt occurs, the MPU:
    1. Completes the instruction being executed
    2. Disables global interrupt enable (no other interrupts are received)
    3. Places the return address on the stack.

- **High-priority Interrupts:**
  - The contents of `W`, `STATUS` and `BSR` registers are automatically saved int orespective shadow registers.
- **Low-priority Interrupts:**
  - These registers must be saved in the ISR.
- RETFIE [s]:
  - When s == 1, MPU also retrieves the contents of `W`, `BSR` and `STATUS` registers
  - RETFIE FAST means s = 1


# Program Organization in MPLAB
- **Program start address:** 0000h.
- **High-priority interrupt address:**  0008h.
- **Low-priority interrupt address:** 0018h.
- Good programming practice; place `goto` statements for high-priority ISRs.

- `ORG` command: its argument is an address. The command after it will be placed in the specified address.
```
ORG  0000h  ; PIC will start here
GOTO start  ; Go to the start tag
```

- In each ISR, interrupt source must be polled.
- After each ISR, interrupt flags must be cleared.

# Round-Robin with Interrupts
- ISRs must do all time-critical tasks that cannot wait (e.g. saving the state of the inputs to a register).
- For all other tasks, it alerts the main routine through flags or buffers. In the main routine, sub-tasks will check for those flags or buffers and act accordingly.
- **NOTE:** Initialize every other part before interrupts.
- **NOTE:** When processing the data received as a result of an interrupt (e.g. a buffered data), that interrupt should be disabled until the end of the processing. Otherwise, data corruption may occur with another interrupt. This situation is called `Data Sharing`.
- **Ring Buffers:**
  - Allocate a fixed amount of memory.
  - Set head and tail pointers to 0.
  - On push, head++. If head == SIZE, head = 0.
  - On pop, tail++. If tail == SIZE, tail = 0.
  - If head == tail, buffer is empty.