# PIC18F Architecture
- Peripherals are devices that perform functionality other than computation itself.

## Harvard Architecture
- Separate program and data bus. They can be in different widths.
- PIC Data memory: a small number of 8-bit registers
- PIC Program memory: 16-bit addresses
- Advantages of Harvard:
  - Separate program and data busses mean that both data and program can be accessed in parallel.
- `RISC` architecture. Usually single cycle per instruction.

## PIC Control Registers
- **STATUS:** Flag register, ALU bits (zero, borrow, carry...)
- **INTCON:** Interrupt control register
- **TRIS:** Tri-state control registers for I/O ports.
- **TXREG:** UART transmit register; next byte to transmit.
- **BSR**: Bank select register; 4-bit wide

## Instruction Speed
Speed = 1/4 clock speed  <==> T_cyc = 4 * T_clk

## Peripherals
- Tri-state I/O pins
- Analog to digital converters (ADC)
- Serial Communications (UART)
- Timers and counters
- Watchdog timers

## PIC 18F4620 Core Features
- Accumulator based machine
- Harvard Architecture
  - 32K x 16bit flash-based **instruction** memory
  - 3968 x 8bit static-ram-based data memory (16 banks)
- 75+8 instructions
- 4 addressing modes (inherent, direct, indirect, relative)
- Speed:
  - 1 cycle/instruction if non-branching instruction
  - 2 cycle/instruction if branching instruction

## Software Point of View
- Memory Mapping:
  - Each bank is 256 bytes long.
  - First half of 0th bank and second half of 15th bank are, respectively, `General Purpose Registers (GPR)` and `Special Function Registers (SFR)`.
  - No need to use BSR for `GPR` and `SFR`. They are called `Access Bank` when combined.

### Addressing modes:
- **Inherent Addressing**:
  - Used by instructions that don't take memory arguments.
- **Literal (immediate) Addressing:**
  - Used by instructions that require an explicit argument.
  - `ADDLW`, `MOVLW`, `CALL`, `GOTO` etc.
- **Direct Addressing:**
  - All or part of the address is specified.
  - 8 bits of 16-bit instruction is used to indentify register file address.
  - Given address can be
    - an address in one of the banks,
    - a location in the `Access Bank`.
- **Indirect Addressing:**
  - There are 4 special function registers `FSRx`.
  - Full 12-bit address is written in one of them. So they are basically **pointers**. It is done by `LFSR` instruction.
  - One of the below registers is used to get the content:
    - **INDFx:** Dereferenced version of FRSx.
    - **POSTINCx:** Dereferenced version of FRSx. Once indirect access is made, pointer is then incremented.
    - **POSTDECx:** Dereferenced version of FRSx. Once indirect access is made, pointer is then decremented.
    - **PREINCx:** Dereferenced version of FRSx. Pointer is incremented and then returned.
    - **PLUSWx:** Dereferenced version of FRSx. Adds the value in `WREG` to the FRSx.

### Instructions
- Instructions have bit `'a'` that determines whether to use banks or Access bank. 
  - When 0, use Access bank.
  - When 1, use BSR.

