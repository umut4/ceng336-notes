# Input - Output
## Memory-mapped vs. Standard I/O
### Memory-mapped
- Peripheral registers occupy addresses in the same address space as memory.
- Requires no additional instruction.
### Standard I/O
- Additional pin on the bus indicates whether a memory or peripheral access.
- No loss of memory addresses to peripherals.

## I/O in PIC
- Ports are the medium of communication between microcontroller and outside world.
- Ports A, B, C and D are bi-direcitonal ports.
- Port E is a 4-bit port with 3 bi-directional pins.
- **TRISx:** controls the configuration of PORTx

### Schmitt Triggerring
Sometimes, noisy inputs can be received. This may create inconsistency in the value of a pin. To overcome this,
- for pin to change from 0 to 1, voltage must rise above a certain high-voltage limit,
- for pin to change from 1 to 0, voltage must drop below a certain low voltage limit.