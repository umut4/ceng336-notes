# Counters And Timers
- **Counter:** counts the input pulses from an external signal.
- **Timer:**
  - Counts the pulses of a fixed, known frequency.
  - Usually the system clock for the processor.
  - Basically, a counter driven by processor clock. Can count down or up.
  - Some are 8, some are 16 bits.
  - Some have `prescalers`, which can be used to select the frequency of the clock input to the timer.
  - Some can connect to external clocks.
  - May generate interrupts on overflow.

- A PIC timer is a register whose value is continually increasing to its MAX value and then starts all over again.
- A timer's input can be chosen to be something other than processor clock.
- A clock's input can be replaced by a signal coming from an event (like an external device). So the counter counts the number of times that event occurs.
- **Measuring Time:**
  - Save begin count.
  - Wait.
  - Store end count.
  - Subtract begin count from end count.
  - Multiply the difference by clock period. Clock period cannot be accessed within the program. It must be specified by the programmer.

## PIC Timers
- **Timer 0:** 
  - 8/16 bit timer/counter with prescalar
  - `T0CON` controls its configuration and prescaler
  - `T0CON<3> (PSA)`: If set, prescaler is disabled (input clock is used), otherwise enabled.
  - `T0CON<2:0> (T0PS2, T0PS1, T0PS0)` control the prescaler value:
    For example, when T0PS<2:0> == B'111', input clock is scaled to 1/256 of its frequency. 
- **Timer 1:** 16 bit timer/counter with prescaler
- **Timer 2:** 8 bit timer with with prescaler and postscaler
- **Timer 3:** 16 bit timer/counter with prescaler
- **Watchdog Timer:** to be discussed later...

### Pre-Scaler
It precedes the clock input of a timer, dividing its frequency by a programmable number.

### Timer Interrupts
- Interrupt on overflow for timers 0, 1 and 3.
- Interrupt when timer 2 cout matches special function register `PR2`.
- Timer0 overflow sets `T0IF` in `INTCON`. This can be used to generate periodic interrupts.

### Initializing Timers
1. Select clock source.
2. Configure the post-scaler, if necessary
3. Select the mode of the timer (8 or 16 bit and accept rising or falling edge)
4. Enable interrupt resources associated with the timer, if they are going to be used.
5. Load an initial value and turn-on the timer.

### Frequency and Microseconds
Goal is to generate an interrupt once every 10ms. Oscillator has frequency 20MHz. Find possible timers.
- 20/4 = 5MHz system clock.
- 10ms = 10000µs = 50000 clock cycles
- Using Timer0 in 8-bit mode:
  - 1:128 prescaler yields 390.625 counts per 10 ms, or 6.553ms between two overflows.
  - 1:256 prescaler yields 195.3125 counts per 10ms, or 13.1072ms between to overflows.
- **Workaround**:
  - Both choices are bad. To overcome this, start the counter from a value, other than 0.
  - For example, if we want the timer to overflow after counting up by 195, we need to pre-load it with 256-195=61.
  - This is still bad because the time between interrupts are still not exactly 10ms.
- **Solution**:
  - Use Timer0 in 16-bit mode, which can count up to 65535 > 50000. So, we don't even need the pre-scaler.

### Reading/Writing from/to Timer0
- In 8 bit mode, regular read/write.
- In 16 bit mode, there is extra logic behind the scenes:
  - When low bits (`TMR0L`) are read, the contents of the high byte is transferred to the `TMR0H` register.
  - This is done to avoid race conditions, in which value in the high byte changes between reading low byte and high byte.
  - The high byte is updated with the contents of TMR0H, when a write occurs to TMR0L, to allow writing all 16 bits of Timer0.
  - To sum up:
    - **Reading:** First TMR0L, then TMR0H.
    - **Writing:** First TMR0H, then TMR0L.

## Watchdog Timer
- It is used to guard against lock-up due to errors.
- It counts up to a certain value.
- When it reaches the target value, a hardware reset is executed, effectively restarting the PIC.
- We need to periodically reset the Watchdog Timer by `CLRWDT`.
- In the homeworks, disable it (clear `WDTEN` bit).